defmodule Euler.Problems.Behaviour do
  @moduledoc """
  General behaviour for solutions to the Euler problems
  """

  @callback call(integer()) :: list(integer()) | integer()
end
