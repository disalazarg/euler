defmodule Euler.Problems.Problem03 do
  @moduledoc """
  The prime factors of 13195 are 5, 7, 13 and 29.

  What is the largest prime factor of the number 600851475143 ?
  """

  @behaviour Euler.Problems.Behaviour

  @impl Euler.Problems.Behaviour
  def call(n) do
    prime(n, [], 2)
    |> Enum.reverse()
  end

  defp prime(n, ps, c)
  defp prime(n, _ps, _c) when n < 1, do: []
  defp prime(1, ps, _c), do: ps
  defp prime(n, ps, p) when rem(n, p) == 0, do: prime(div(n, p), [p | ps], 2)
  defp prime(n, ps, p) when p > n, do: [n | ps]
  defp prime(n, ps, p), do: prime(n, ps, p + 1)
end
