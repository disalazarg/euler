defmodule Euler.Problem_7 do
  def call n do
    1..20_000
      |> Stream.flat_map(&([6*&1 - 1, 6*&1 + 1]))
      |> Stream.filter(&is_prime/1)
      |> Enum.at(n - 3)
  end

  def is_prime(n) when 
    rem(n, 2) == 0 or 
    rem(n, 3) == 0, 
      do: false

  def is_prime(n) do
    4..(:math.sqrt(n) |> round)
      |> Stream.map(&(rem(n, &1) != 0))
      |> Enum.all?
  end
end