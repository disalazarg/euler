defmodule Euler.Problem_10 do
  def call n do
    rcall(5, n, 5)
  end

  def rcall(sum, limit, cand) when cand >= limit, do: sum
  def rcall(sum, limit, cand) do
    if is_prime(cand) do
      rcall(sum + cand, limit, cand + 2)
    else
      rcall(sum, limit, cand + 2)
    end
  end

  def is_prime(n) when 
    rem(n, 2) == 0 or 
    rem(n, 3) == 0, 
      do: false

  def is_prime(n) do
    4..(:math.sqrt(n) |> round)
      |> Stream.map(&(rem(n, &1) != 0))
      |> Enum.all?
  end
end