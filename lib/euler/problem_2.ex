defmodule Euler.Problem_2 do
  def call n do
    fibs(n)
      |> Enum.filter(fn x -> rem(x, 2) == 0 end)
      |> Enum.sum
  end

  def fibs n do
    rcall([], n, 1, 2)
      |> Enum.reverse
  end

  defp rcall(list, n, a, _b) when n < a do
    list
  end

  defp rcall(list, n, a, b) do
    rcall([a | list], n, b, (a + b))
  end
end