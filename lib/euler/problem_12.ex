defmodule Euler.Problem_12 do
  def call n do
    rcall(n, 2, 1)
  end

  defp rcall(n, c, t) do
    if divs(t) > n,
      do:   t,
      else: rcall(n, c + 1, t + c)
  end

  def divs n do
    1..n
      |> Enum.filter(fn x -> rem(n, x) == 0 end)
      |> Enum.reduce(0, fn _x, acc -> acc + 1 end)
  end
end