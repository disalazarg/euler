defmodule Euler.Problem_4 do
  def call n do
    min..max = (round :math.pow(10, n - 1))..(round :math.pow(10, n) - 1)

    for(x <- min..max, y <- x..max, is_palindrome(x * y), do: x * y)
      |> Enum.max
  end

  def is_palindrome n do
    Integer.to_string(n)
      |> (fn(x) -> x == String.reverse x end).()
  end
end