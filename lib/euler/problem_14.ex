defmodule Euler.Problem_14 do
  def call n do
    1..n
      |> Stream.map(fn x -> {x, chain(x)} end)
      |> Enum.max_by(fn {_x, cx} -> cx end)
      |> elem(0)
  end

  def chain(n), do: rchain(n, 0)

  defp rchain(1, c), do: c + 1
  defp rchain(n, c), do: rchain(next(n), c + 1)

  def next(n) when rem(n, 2) == 0, do: div(n, 2)
  def next(n) when rem(n, 2) == 1, do: 3*n + 1
end