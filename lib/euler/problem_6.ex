defmodule Euler.Problem_6 do
  def call n do
    square_sum(n) - sum_square(n)
  end

  def sum_square(n), do: (1..n |> Enum.map(&square/1) |> Enum.sum)
  def square_sum(n), do: (1..n |> Enum.sum |> square)

  defp square(n), do: n * n
end