defmodule Euler.Problem_3 do
  def call n do
    pfacts(n)
      |> List.first
  end

  def pfacts n do
    {list, np} = {[], n}
      |> rcall(2)
      |> rcall(3)

    {list, 1} = 1..ubound(n)
      |> Enum.reduce({list, np}, fn x, acc -> (acc |> rcall(6*x - 1) |> rcall(6*x + 1)) end)

    list
  end

  def ubound n do
    :math.sqrt(n)
      |> round
      |> div(6)
  end

  defp rcall({list, 1}, _), do: {list, 1}
  defp rcall({list, n}, cand) do
    case rem(n, cand) do
      0 -> rcall({[cand | list], div(n, cand)}, cand)
      _ -> {list, n}
    end
  end
end