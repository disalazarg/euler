defmodule Euler.Problem_9 do
  def call n do
    for(x <- 1..n, y <- 1..n, x <= y and x + y + z(x, y) == n and fz(x, y), do: (x * y * round(z(x, y))) |> to_string)
      |> List.first
      |> String.to_integer
  end

  def z(x, y), do: :math.sqrt(x*x + y*y)
  def fz(x, y), do: round(z(x, y)) == z(x, y)
end