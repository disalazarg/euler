defmodule Euler.Problem_5 do
  def call n do
    1..n
      |> Enum.reduce([], &(func(&1, &2) |> fadd(&2)))
      |> Enum.reduce(1, &(&1 * &2))
  end

  def func(n, list) do
    list
      |> Enum.reduce(n, &fdiv/2)
  end

  defp fadd(1, list), do: list
  defp fadd(n, list), do: [n | list]

  defp fdiv(a, b) when rem(b, a) == 0, do: div(b, a)
  defp fdiv(_, b), do: b
end