defmodule Euler do
  def main _args do
    IO.puts Euler.Problem_1.call 1_000
    IO.puts Euler.Problem_2.call 4_000_000
    IO.puts Euler.Problem_3.call 600_851_475_143
    IO.puts Euler.Problem_4.call 3
    IO.puts Euler.Problem_5.call 20
    IO.puts Euler.Problem_6.call 100
    IO.puts Euler.Problem_7.call 10001
    IO.puts Euler.Problem_8.call 13
    IO.puts Euler.Problem_9.call 1000
    IO.puts Euler.Problem_10.call 2_000
    IO.puts Euler.Problem_13.call
    IO.puts Euler.Problem_14.call 1_000_000
  end
end
