defmodule EulerTest do
  use ExUnit.Case
  doctest Euler

  test "the truth" do
    assert 1 + 1 == 2
    refute 2 > 3
  end

  test "problem #1" do
    assert Euler.Problem_1.call(10) == 23
  end

  test "problem #2" do
    assert Euler.Problem_2.fibs(10) == [1,2,3,5,8]
    assert Euler.Problem_2.call(10) == 10
  end

  test "problem #3" do
    assert Euler.Problem_3.pfacts(8)     == [2,2,2]
    assert Euler.Problem_3.pfacts(13195) == [29,13,7,5]
    assert Euler.Problem_3.call(13195)   == 29
  end

  test "problem #4" do
    assert Euler.Problem_4.is_palindrome(9009) == true
    assert Euler.Problem_4.call(2)             == 9009
  end

  test "problem #5" do
    assert Euler.Problem_5.call(10) == 2520
  end

  test "problem #6" do
    assert Euler.Problem_6.sum_square(10) == 385
    assert Euler.Problem_6.square_sum(10) == 3025
    assert Euler.Problem_6.call(10)       == 2640
  end

  test "problem #7" do
    assert Euler.Problem_7.is_prime(4)  == false
    assert Euler.Problem_7.is_prime(5)  == true
    assert Euler.Problem_7.is_prime(25) == false
    assert Euler.Problem_7.call(6)      == 13
  end

  test "problem #8" do
    assert Euler.Problem_8.call(4) == 5832
  end

  test "problem #9" do
    assert Euler.Problem_9.call(12) == 60
  end

  test "problem #10" do
    assert Euler.Problem_10.call(10) == 17
  end

  test "problem #12" do
    assert Euler.Problem_12.divs(1)  == 1
    assert Euler.Problem_12.divs(6)  == 4
    assert Euler.Problem_12.divs(21) == 4
    assert Euler.Problem_12.call(5)  == 28
  end

  test "problem #13" do
    assert Euler.Problem_13.call > 0
  end

  test "problem #14" do
    assert Euler.Problem_14.next(3)   == 10
    assert Euler.Problem_14.next(4)   == 2
    assert Euler.Problem_14.chain(13) == 10 #[1, 2, 4, 8, 16, 5, 10, 20, 40, 13]
    assert Euler.Problem_14.call(10)  == 9
  end
end
