defmodule Euler.ProblemCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use PropCheck, default_opts: [numtests: 100]

      import Euler.ProblemCase.Generators
    end
  end

  defmodule Generators do
    use PropCheck

    def non_zero, do: such_that(n <- non_neg_integer(), when: n > 0)
  end
end
