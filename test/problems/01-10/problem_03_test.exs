defmodule Euler.Problems.Problem03Test do
  use Euler.ProblemCase

  alias Euler.Problems.Problem03

  # Properties
  property "is a decomposition of the original" do
    forall n <- non_zero() do
      lst = Problem03.call(n)

      assert Enum.reduce(lst, 1, &*/2) == n
    end
  end

  # Regular unit tests
  test "the example case works" do
    assert Problem03.call(13195) == [5, 7, 13, 29]
  end
end
