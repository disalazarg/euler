defmodule Euler.Problems.Problem02Test do
  use Euler.ProblemCase

  alias Euler.Problems.Problem02

  property "is an even number" do
    forall n <- non_zero() do
      sum = Problem02.call(n)

      assert rem(sum, 2) == 0
    end
  end

  # Regular unit tests
  test "the example case works" do
    assert Problem02.call(100) == 2 + 8 + 34
  end
end
